function myFunction(id) {
    let x = document.getElementById('myDIV' + id);
    let rotate = document.getElementById('rotation' + id);

    rotate.classList.toggle("down");

    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }